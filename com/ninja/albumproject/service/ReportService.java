/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ninja.albumproject.service;

import com.ninja.albumproject.Dao.SaleDao;
import com.ninja.albumproject.model.ReportSale;
import java.util.List;

/**
 *
 * @author USER
 */
public class ReportService {
        public List<ReportSale> getReportSaleByDay(){
        SaleDao dao = new SaleDao();
        return dao.getDayReport();
    }
            public List<ReportSale> getReportSaleByMonth(int year){
        SaleDao dao = new SaleDao();
        return dao.getMonthReport(year);
    }
            public List<ReportSale> getReportSaleByYear(){
        SaleDao dao = new SaleDao();
        return dao.getYearReport();
    }
}
